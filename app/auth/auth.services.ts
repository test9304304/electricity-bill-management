import { compare } from "bcrypt";
import jwt from "jsonwebtoken";

import userServices from "../users/user.services";
import { IUser } from "../users/user.types";

import roleServices from "../roles/role.services";
import { roleResponses } from "../roles/role.responses";

import { ICredentials } from "./auth.types";

import { authResponses } from "./auth.responses";

import { encrypt } from "../utils/encrypt";

const login = async (credentials: ICredentials) => {
	try {
		const user = await userServices.findOne({ email: credentials.email });
		const didMatch = await compare(credentials.password, user.password);
		if (!didMatch) throw authResponses.INVALID_CREDENTIALS;

		const { email, role } = user;
		const roleDoc = await roleServices.findOne({ _id: role });
		if (!roleDoc) throw roleResponses.NOT_FOUND;

		const { JWT_SECRET } = process.env;
		const token = jwt.sign({ email, role: roleDoc._id }, JWT_SECRET || "");

		return { token, email, role: roleDoc.roleName };
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw authResponses.INVALID_CREDENTIALS;
	}
};

const signup = async (userData: IUser) => {
	try {
		const alreadyRegistered = await userServices.findOne(userData, true);
		if (alreadyRegistered) throw authResponses.USER_ALREADY_REGISTERED;

		const encryptedPass = await encrypt(userData.password);
		userData = { ...userData, password: encryptedPass };
		userServices.insertOne(userData);

		return userData;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw authResponses.REGISTRATION_FAILED;
	}
};

export default {
	login,
	signup,
};
