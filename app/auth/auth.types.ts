import { z } from "zod";

import { ZUser } from "../users/user.types";

export const ZCredentials = ZUser.pick({ email: true, password: true });
export interface ICredentials extends z.infer<typeof ZCredentials> {}

export const ZSignupData = ZUser;
export interface SignupData extends z.infer<typeof ZSignupData> {}
