import { billModel } from "./bill.schema";

import { IBill } from "./bill.types";

const find = async (query: Partial<IBill>) => await billModel.find(query);

const findOne = async (query: Partial<IBill>) => await billModel.findOne(query);

const insertOne = async (data: IBill) => await billModel.create(data);

const insertMany = async (data: IBill[]) => await billModel.insertMany(data);

const findOneAndUpdate = async (findQuery: Partial<IBill>, updateObj: Partial<IBill>) =>
	await billModel.findOneAndUpdate(findQuery, updateObj);

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
};
