import { IResponses } from "../utils/base-schema";

export const billResponses: IResponses = {
	SERVER_ERR: {
		statusCode: 500,
		message: "BILL: SERVER ERR",
	},
	NOT_FOUND: {
		statusCode: 404,
		message: "BILL: NOT FOUND",
	},
	UPDATE_FAILED: {
		statusCode: 400,
		message: "BILL: UPDATE FAILED",
	},
	UPDATE_SUCCESSFUL: {
		statusCode: 200,
		message: "BILL: UPDATE SUCCESSFUL",
	},
	DELETE_FAILED: {
		statusCode: 400,
		message: "BILL: DELETE FAILED",
	},
	DELETE_SUCCESSFUL: {
		statusCode: 200,
		message: "BILL: DELETE SUCCESSFUL",
	},
	INSERT_FAILED: {
		statusCode: 400,
		message: "BILL: INSERT FAILED",
	},
	EMAIL_FAILED: {
		statusCode: 404,
		message: "BILL: EMAIL FAILED TO SEND",
	},
};
