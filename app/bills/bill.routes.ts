import { Router } from "express";

import { Route } from "../routes/routes.types";
import { ResponseHandler } from "../utils/response-handler";
import { upload } from "../utils/file-uploader";

import billServices from "./bill.services";

const router = Router();

router.get("/:id?", async (req, res, next) => {
	try {
		const { id } = req.params;
		const params = id ? { _id: id } : {};
		const result = await billServices.find({ ...params, ...req.query });
		res.send(new ResponseHandler(result));
	} catch (e) {
		next(e);
	}
});

router.get("/user/:userId?", async (req, res, next) => {
	try {
		const { userId } = req.params;
		const result = await billServices.find({ userId });
		res.send(new ResponseHandler(result));
	} catch (e) {
		next(e);
	}
});

router.post("/meter/:meterId", upload.array("photos", 5), async (req, res, next) => {
	try {
		const { meterId } = req.params;
		const data = req.body;
		const photos = req.files as Express.Multer.File[];
		const result = await billServices.insertOne(data, meterId, photos);
		res.send(new ResponseHandler(result));
	} catch (e) {
		next(e);
	}
});

router.patch("/:id", async (req, res, next) => {
	try {
		const { id } = req.params;
		const result = await billServices.findOneAndUpdate({ _id: id }, req.body);
		res.send(new ResponseHandler(result));
	} catch (e) {
		next(e);
	}
});

router.delete("/:id", async (req, res, next) => {
	try {
		const { id } = req.params;
		const result = await billServices.deleteOne({ _id: id });
		res.send(new ResponseHandler(result));
	} catch (e) {
		next(e);
	}
});

export default new Route("/api/bill", router);
