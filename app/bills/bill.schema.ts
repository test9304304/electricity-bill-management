import { Schema, model } from "mongoose";
import { BaseSchema } from "../utils/base-schema";

import { BillDocument } from "./bill.types";

const billSchema = new BaseSchema({
	userId: { type: Schema.Types.ObjectId, ref: "users", required: true },
	readingId: { type: Schema.Types.ObjectId, ref: "readings", required: true },
	totalAmount: { type: Number, required: true },
});

export const billModel = model<BillDocument>("bills", billSchema);
