import billRepo from "./bill.repo";

import { IBill } from "./bill.types";
import { billResponses } from "./bill.responses";
import { generatePDF } from "../utils/pdf-generator";
import { sendBillEmail } from "../utils/send-mail";

import readingServices from "../readings/reading.services";
import { IReading } from "../readings/reading.types";
import { readingResponses } from "../readings/reading.responses";

import boardServices from "../boards/board.services";
import { boardResponses } from "../boards/board.responses";

import meterServices from "../meters/meter.services";
import { meterResponses } from "../meters/meter.responses";

import userServices from "../users/user.services";
import { userResponses } from "../users/user.responses";

const find = async (query: Partial<IBill>) => await billRepo.find(query);

async function findOne(query: Partial<IBill>) {
	try {
		const result = await billRepo.findOne(query);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw billResponses.NOT_FOUND;
	}
}

const insertOne = async (data: Partial<IReading>, meterId: string, photos: Express.Multer.File[]) => {
	try {
		if (data.status === "revisit") {
			const reading = await readingServices.insertOne({
				...data,
				meterId,
			});
			if (!reading) throw readingResponses.INSERT_FAILED;

			return reading;
		}

		const meter = await meterServices.findOne({ _id: meterId });
		if (!meter) throw meterResponses.NOT_FOUND;

		const user = await userServices.findOne({ _id: meter.userId });
		if (!user) throw userResponses.NOT_FOUND;

		const board = await boardServices.findOne({ _id: meter.boardId });
		if (!board) throw boardResponses.NOT_FOUND;

		const photoUrls: string[] = photos.map((photo: Express.Multer.File) => photo.path);
		const reading = await readingServices.insertOne({
			...data,
			meterId,
			photos: photoUrls,
		});
		if (!reading) throw readingResponses.INSERT_FAILED;

		const rates = board.rates[meter.baseType];
		const baseRate = rates.base;
		const discount = meter.subType === "solar" ? rates.discount : 0;

		const amount = baseRate * reading.unitsConsumed;
		const discountAmt = discount !== 0 ? amount * (discount / 100) : 0;
		const totalAmount = amount - discountAmt;

		const billDoc = { userId: meter.userId, readingId: reading._id, totalAmount };
		const bill = await billRepo.insertOne(billDoc);

		const billData = {
			billId: bill._id,
			userId: meter.userId,
			meterId,
			unitsConsumed: reading.unitsConsumed,
			baseRate,
			discount,
			totalAmount,
		};
		const pdfPath = generatePDF(billData);
		const email = await sendBillEmail(user.email, bill._id, pdfPath);
		if (!email) throw billResponses.EMAIL_FAILED;
	
		return bill;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw billResponses.INSERT_FAILED;
	}
};

const insertMany = async (data: IBill[]) => {
	try {
		const result = await billRepo.insertMany(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw billResponses.INSERT_FAILED;
	}
};

const findOneAndUpdate = async (findQuery: Partial<IBill>, updateObj: Partial<IBill>) => {
	try {
		const result = await billRepo.findOneAndUpdate(findQuery, updateObj);
		return billResponses.UPDATE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw billResponses.UPDATE_FAILED;
	}
};

const deleteOne = async (query: Partial<IBill>) => {
	try {
		const result = await billRepo.findOneAndUpdate(query, { isDeleted: true });
		return billResponses.DELETE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw billResponses.SERVER_ERR;
	}
};

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
	deleteOne,
};
