import z from "zod";
import { Document } from "mongoose";
import { ZBase } from "../utils/base-schema";

export const ZBill = ZBase.extend({
	userId: z.string(),
	readingId: z.string(),
	totalAmount: z.number(),
});

export interface IBill extends z.infer<typeof ZBill> {}
export type BillDocument = Document & IBill;

export interface IBillData {
	billId: string;
	userId: string;
	meterId: string;
	unitsConsumed: number;
	baseRate: number;
	discount: number;
	totalAmount: number;
}
