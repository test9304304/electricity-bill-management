import { boardModel } from "./board.schema";

import { IBoard } from "./board.types";

const find = async (query: Partial<IBoard>) => await boardModel.find(query);

const findOne = async (query: Partial<IBoard>) => await boardModel.findOne(query);

const insertOne = async (data: IBoard) => await boardModel.create(data);

const insertMany = async (data: IBoard[]) => await boardModel.insertMany(data);

const findOneAndUpdate = async (findQuery: Partial<IBoard>, updateObj: Partial<IBoard>) =>
	await boardModel.findOneAndUpdate(findQuery, updateObj);

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
};
