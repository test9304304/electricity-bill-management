import { IResponses } from "../utils/base-schema";

export const boardResponses: IResponses = {
	SERVER_ERR: {
		statusCode: 500,
		message: "BOARD: SERVER ERR",
	},
	NOT_FOUND: {
		statusCode: 404,
		message: "BOARD: NOT FOUND",
	},
	UPDATE_FAILED: {
		statusCode: 400,
		message: "BOARD: UPDATE FAILED",
	},
	UPDATE_SUCCESSFUL: {
		statusCode: 200,
		message: "BOARD: UPDATE SUCCESSFUL",
	},
	DELETE_FAILED: {
		statusCode: 400,
		message: "BOARD: DELETE FAILED",
	},
	DELETE_SUCCESSFUL: {
		statusCode: 200,
		message: "BOARD: DELETE SUCCESSFUL",
	},
	INSERT_FAILED: {
		statusCode: 400,
		message: "BOARD: INSERT FAILED",
	},
};
