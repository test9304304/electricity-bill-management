import { Schema, model } from "mongoose";
import { BaseSchema } from "../utils/base-schema";

import { BoardDocument } from "./board.types";

const boardSchema = new BaseSchema({
	name: { type: String, required: true },
	rates: {
		household: {
			base: { type: Number, required: true },
			discount: { type: Number, required: true },
		},
		industrial: {
			base: { type: Number, required: true },
			discount: { type: Number, required: true },
		},
	},
});

export const boardModel = model<BoardDocument>("boards", boardSchema);
