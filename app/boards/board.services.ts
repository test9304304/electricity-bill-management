import ticketRepo from "./board.repo";

import { IBoard } from "./board.types";
import { boardResponses } from "./board.responses";

const find = async (query: Partial<IBoard>) => await ticketRepo.find(query);

async function findOne(query: Partial<IBoard>) {
	try {
		const result = await ticketRepo.findOne(query);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw boardResponses.NOT_FOUND;
	}
}

const insertOne = async (data: IBoard) => {
	try {
		const result = await ticketRepo.insertOne(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw boardResponses.INSERT_FAILED;
	}
};

const insertMany = async (data: IBoard[]) => {
	try {
		const result = await ticketRepo.insertMany(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw boardResponses.INSERT_FAILED;
	}
};

const findOneAndUpdate = async (findQuery: Partial<IBoard>, updateObj: Partial<IBoard>) => {
	try {
		const result = await ticketRepo.findOneAndUpdate(findQuery, updateObj);
		return boardResponses.UPDATE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw boardResponses.UPDATE_FAILED;
	}
};

const deleteOne = async (query: Partial<IBoard>) => {
	try {
		const result = await ticketRepo.findOneAndUpdate(query, { isDeleted: true });
		return boardResponses.DELETE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw boardResponses.SERVER_ERR;
	}
};

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
	deleteOne,
};
