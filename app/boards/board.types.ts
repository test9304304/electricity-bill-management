import z from "zod";
import { Document } from "mongoose";
import { ZBase } from "../utils/base-schema";

export const ZBoard = ZBase.extend({
	name: z.string(),
	rates: z.object({
		household: z.object({
			base: z.number(),
			discount: z.number(),
		}),
		industrial: z.object({
			base: z.number(),
			discount: z.number(),
		}),
	}),
});

export interface IBoard extends z.infer<typeof ZBoard> {}
export type BoardDocument = Document & IBoard;
