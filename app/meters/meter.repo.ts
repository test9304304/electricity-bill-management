import { meterModel } from "./meter.schema";

import { IMeter } from "./meter.types";

const find = async (query: Partial<IMeter>) => await meterModel.find(query);

const findOne = async (query: Partial<IMeter>) => await meterModel.findOne(query);

const insertOne = async (data: IMeter) => await meterModel.create(data);

const insertMany = async (data: IMeter[]) => await meterModel.insertMany(data);

const findOneAndUpdate = async (findQuery: Partial<IMeter>, updateObj: Partial<IMeter>) =>
	await meterModel.findOneAndUpdate(findQuery, updateObj);

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
};
