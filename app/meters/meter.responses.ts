import { IResponses } from "../utils/base-schema";

export const meterResponses: IResponses = {
	SERVER_ERR: {
		statusCode: 500,
		message: "METER: SERVER ERR",
	},
	NOT_FOUND: {
		statusCode: 404,
		message: "METER: NOT FOUND",
	},
	UPDATE_FAILED: {
		statusCode: 400,
		message: "METER: UPDATE FAILED",
	},
	UPDATE_SUCCESSFUL: {
		statusCode: 200,
		message: "METER: UPDATE SUCCESSFUL",
	},
	DELETE_FAILED: {
		statusCode: 400,
		message: "METER: DELETE FAILED",
	},
	DELETE_SUCCESSFUL: {
		statusCode: 200,
		message: "METER: DELETE SUCCESSFUL",
	},
	INSERT_FAILED: {
		statusCode: 400,
		message: "METER: INSERT FAILED",
	},
};
