import { Schema, model } from "mongoose";
import { BaseSchema } from "../utils/base-schema";

import { MeterDocument } from "./meter.types";

const meterSchema = new BaseSchema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: "users",
		required: true,
	},
	baseType: {
		type: String,
		enum: ["household", "industrial"],
		required: true,
	},
	subType: {
		type: String,
		enum: ["solar", "regular"],
		required: true,
	},
	boardId: {
		type: Schema.Types.ObjectId,
		ref: "boards",
		required: true,
	},
});

export const meterModel = model<MeterDocument>("meters", meterSchema);
