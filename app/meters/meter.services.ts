import ticketRepo from "./meter.repo";

import { IMeter } from "./meter.types";
import { meterResponses } from "./meter.responses";

const find = async (query: Partial<IMeter>) => await ticketRepo.find(query);

async function findOne(query: Partial<IMeter>) {
	try {
		const result = await ticketRepo.findOne(query);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw meterResponses.NOT_FOUND;
	}
}

const insertOne = async (data: IMeter) => {
	try {
		const result = await ticketRepo.insertOne(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw meterResponses.INSERT_FAILED;
	}
};

const insertMany = async (data: IMeter[]) => {
	try {
		const result = await ticketRepo.insertMany(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw meterResponses.INSERT_FAILED;
	}
};

const findOneAndUpdate = async (findQuery: Partial<IMeter>, updateObj: Partial<IMeter>) => {
	try {
		const result = await ticketRepo.findOneAndUpdate(findQuery, updateObj);
		return meterResponses.UPDATE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw meterResponses.UPDATE_FAILED;
	}
};

const deleteOne = async (query: Partial<IMeter>) => {
	try {
		const result = await ticketRepo.findOneAndUpdate(query, { isDeleted: true });
		return meterResponses.DELETE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw meterResponses.SERVER_ERR;
	}
};

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
	deleteOne,
};
