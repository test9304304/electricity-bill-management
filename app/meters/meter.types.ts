import z from "zod";
import { Document } from "mongoose";
import { ZBase } from "../utils/base-schema";

export const ZMeter = ZBase.extend({
    userId: z.string(),
	baseType: z.enum(["household", "industrial"]),
	subType: z.enum(["solar", "regular"]),
	boardId: z.string(),
});

export interface IMeter extends z.infer<typeof ZMeter> {}
export type MeterDocument = Document & IMeter;
