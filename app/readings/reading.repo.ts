import { readingModel } from "./reading.schema";

import { IReading } from "./reading.types";

const find = async (query: Partial<IReading>) => await readingModel.find(query);

const findOne = async (query: Partial<IReading>) => await readingModel.findOne(query);

const insertOne = async (data: Partial<IReading>) => await readingModel.create(data);

const insertMany = async (data: Partial<IReading>[]) => await readingModel.insertMany(data);

const findOneAndUpdate = async (findQuery: Partial<IReading>, updateObj: Partial<IReading>) =>
	await readingModel.findOneAndUpdate(findQuery, updateObj);

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
};
