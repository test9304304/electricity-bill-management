import { IResponses } from "../utils/base-schema";

export const readingResponses: IResponses = {
	SERVER_ERR: {
		statusCode: 500,
		message: "READING: SERVER ERR",
	},
	NOT_FOUND: {
		statusCode: 404,
		message: "READING: NOT FOUND",
	},
	UPDATE_FAILED: {
		statusCode: 400,
		message: "READING: UPDATE FAILED",
	},
	UPDATE_SUCCESSFUL: {
		statusCode: 200,
		message: "READING: UPDATE SUCCESSFUL",
	},
	DELETE_FAILED: {
		statusCode: 400,
		message: "READING: DELETE FAILED",
	},
	DELETE_SUCCESSFUL: {
		statusCode: 200,
		message: "READING: DELETE SUCCESSFUL",
	},
	INSERT_FAILED: {
		statusCode: 400,
		message: "READING: INSERT FAILED",
	},
};
