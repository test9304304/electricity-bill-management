import { Schema, model } from "mongoose";
import { BaseSchema } from "../utils/base-schema";

import { ReadingDocument } from "./reading.types";

const readingSchema = new BaseSchema({
	meterId: { type: Schema.Types.ObjectId, ref: "meters", required: true },
	unitsConsumed: { type: Number, required: true },
	photos: [{ type: String, required: true }],
	status: { type: String, enum: ["approved", "revisit"], required: true },
});

export const readingModel = model<ReadingDocument>("readings", readingSchema);
