import readingRepo from "./reading.repo";

import { IReading } from "./reading.types";
import { readingResponses } from "./reading.responses";

const find = async (query: Partial<IReading>) => await readingRepo.find(query);

async function findOne(query: Partial<IReading>) {
	try {
		const result = await readingRepo.findOne(query);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw readingResponses.NOT_FOUND;
	}
}

const insertOne = async (data: Partial<IReading>) => {
	try {
		const result = await readingRepo.insertOne(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw readingResponses.INSERT_FAILED;
	}
};

const findOneAndUpdate = async (findQuery: Partial<IReading>, updateObj: Partial<IReading>) => {
	try {
		const result = await readingRepo.findOneAndUpdate(findQuery, updateObj);
		return readingResponses.UPDATE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw readingResponses.UPDATE_FAILED;
	}
};

const deleteOne = async (query: Partial<IReading>) => {
	try {
		const result = await readingRepo.findOneAndUpdate(query, { isDeleted: true });
		return readingResponses.DELETE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw readingResponses.SERVER_ERR;
	}
};

export default {
	find,
	findOne,
	insertOne,
	findOneAndUpdate,
	deleteOne,
};
