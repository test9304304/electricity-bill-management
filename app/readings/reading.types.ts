import z from "zod";
import { Document } from "mongoose";
import { ZBase } from "../utils/base-schema";

export const ZReading = ZBase.extend({
	meterId: z.string(),
	unitsConsumed: z.number(),
	photos: z.array(z.string().url()),
	status: z.enum(["approved", "revisit"]),
});

export interface IReading extends z.infer<typeof ZReading> {}
export type ReadingDocument = Document & IReading;
