import { roleModel } from "./role.schema";

import { IRole } from "./role.types";

const find = async (query: Partial<IRole>) => await roleModel.find(query);

const findOne = async (query: Partial<IRole>) => await roleModel.findOne(query);

const insertOne = async (data: IRole) => await roleModel.create(data);

const insertMany = async (data: IRole[]) => await roleModel.insertMany(data);

const findOneAndUpdate = async (findQuery: Partial<IRole>, updateObj: Partial<IRole>) =>
	await roleModel.findOneAndUpdate(findQuery, updateObj);

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
};
