import { model } from "mongoose";
import { BaseSchema } from "../utils/base-schema";

import { RoleDocument } from "./role.types";

const roleSchema = new BaseSchema({
	roleName: { type: String, required: true, unique: true },
});

export const roleModel = model<RoleDocument>("roles", roleSchema);
