import roleRepo from "./role.repo";

import { IRole } from "./role.types";
import { roleResponses } from "./role.responses";

const find = async (query: Partial<IRole>) => await roleRepo.find(query);

async function findOne(query: Partial<IRole>) {
	try {
		const result = await roleRepo.findOne(query);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw roleResponses.NOT_FOUND;
	}
}

const insertOne = async (data: IRole) => {
	try {
		const result = await roleRepo.insertOne(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw roleResponses.INSERT_FAILED;
	}
};

const insertMany = async (data: IRole[]) => {
	try {
		const result = await roleRepo.insertMany(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw roleResponses.INSERT_FAILED;
	}
};

const deleteOne = async (query: Partial<IRole>) => {
	try {
		await roleRepo.findOneAndUpdate(query, { isDeleted: true });
		return roleResponses.DELETE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw roleResponses.DELETE_FAILED;
	}
};

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	deleteOne,
};
