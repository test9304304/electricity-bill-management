import z from "zod";
import { Document } from "mongoose";
import { ZBase } from "../utils/base-schema";

export const ZRole = ZBase.extend({
	roleName: z.string(),
});

export interface IRole extends z.infer<typeof ZRole> {}
export type RoleDocument = Document & IRole;

export enum ROLE {
	admin = "6655b9881196eb4a5ec44822",
	supervisor = "6655b9881196eb4a5ec44823",
	fieldWorker = "6655b9881196eb4a5ec44824",
	boardAdmin = "6655b9881196eb4a5ec44825",
	boardMember = "6655b9881196eb4a5ec44826",
	user = "6655b9881196eb4a5ec44827",
}
