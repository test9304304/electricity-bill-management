import { match } from "path-to-regexp";

import roleRoutes from "../roles/role.routes";
import userRoutes from "../users/user.routes";
import authRoutes from "../auth/auth.routes";

import { ExcludedRoutes, Route } from "./routes.types";

export const routes: Route[] = [roleRoutes, userRoutes, authRoutes];

export const excludedRoutes: ExcludedRoutes = [
	{ path: match("/api/auth/login"), method: "POST" },
	{ path: match("/api/auth/signup"), method: "POST" },
];
