import { ticketModel } from "./ticket.schema";

import { ITicket } from "./ticket.types";

const find = async (query: Partial<ITicket>) => await ticketModel.find(query);

const findOne = async (query: Partial<ITicket>) => await ticketModel.findOne(query);

const insertOne = async (data: Partial<ITicket>) => await ticketModel.create(data);

const insertMany = async (data: Partial<ITicket>[]) => await ticketModel.insertMany(data);

const findOneAndUpdate = async (findQuery: Partial<ITicket>, updateObj: Partial<ITicket>) =>
	await ticketModel.findOneAndUpdate(findQuery, updateObj);

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
};
