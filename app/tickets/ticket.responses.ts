import { IResponses } from "../utils/base-schema";

export const ticketResponses: IResponses = {
	SERVER_ERR: {
		statusCode: 500,
		message: "TICKET: SERVER ERR",
	},
	NOT_FOUND: {
		statusCode: 404,
		message: "TICKET: NOT FOUND",
	},
	UPDATE_FAILED: {
		statusCode: 400,
		message: "TICKET: UPDATE FAILED",
	},
	UPDATE_SUCCESSFUL: {
		statusCode: 200,
		message: "TICKET: UPDATE SUCCESSFUL",
	},
	DELETE_FAILED: {
		statusCode: 400,
		message: "TICKET: DELETE FAILED",
	},
	DELETE_SUCCESSFUL: {
		statusCode: 200,
		message: "TICKET: DELETE SUCCESSFUL",
	},
    INSERT_SUCCESSFUL: {
		statusCode: 200,
		message: "TICKET: INSERT SUCCESSFUL",
	},
	INSERT_FAILED: {
		statusCode: 400,
		message: "TICKET: INSERT FAILED",
	},
};
