import { Schema, model } from "mongoose";
import { BaseSchema } from "../utils/base-schema";

import { TicketDocument } from "./ticket.types";

const ticketSchema = new BaseSchema({
	userId: { type: Schema.Types.ObjectId, ref: "users", required: true },
	billId: { type: Schema.Types.ObjectId, ref: "bills", required: true },
	issueDescription: { type: String, required: true },
	status: { type: String, enum: ["pending", "closed"], default: "pending" },
});

export const ticketModel = model<TicketDocument>("tickets", ticketSchema);
