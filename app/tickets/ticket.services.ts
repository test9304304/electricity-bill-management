import ticketRepo from "./ticket.repo";

import { ITicket } from "./ticket.types";
import { ticketResponses } from "./ticket.responses";

const find = async (query: Partial<ITicket>) => await ticketRepo.find(query);

async function findOne(query: Partial<ITicket>) {
	try {
		const result = await ticketRepo.findOne(query);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw ticketResponses.NOT_FOUND;
	}
}

const insertOne = async (data: Partial<ITicket>) => {
	try {
		const result = await ticketRepo.insertOne(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw ticketResponses.INSERT_FAILED;
	}
};

const insertMany = async (data: Partial<ITicket>[]) => {
	try {
		const result = await ticketRepo.insertMany(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw ticketResponses.INSERT_FAILED;
	}
};

const findOneAndUpdate = async (findQuery: Partial<ITicket>, updateObj: Partial<ITicket>) => {
	try {
		const result = await ticketRepo.findOneAndUpdate(findQuery, updateObj);
		return ticketResponses.UPDATE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw ticketResponses.UPDATE_FAILED;
	}
};

const deleteOne = async (query: Partial<ITicket>) => {
	try {
		const result = await ticketRepo.findOneAndUpdate(query, { isDeleted: true });
		return ticketResponses.DELETE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw ticketResponses.SERVER_ERR;
	}
};

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
	deleteOne,
};
