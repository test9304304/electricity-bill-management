import z from "zod";
import { Document } from "mongoose";
import { ZBase } from "../utils/base-schema";

export const ZTicket = ZBase.extend({
	userId: z.string(),
	billId: z.string(),
	issueDescription: z.string(),
	status: z.enum(["pending", "closed"]),
});

export interface ITicket extends z.infer<typeof ZTicket> {}
export type TicketDocument = Document & ITicket;
