import { userModel } from "./user.schema";

import { IUser } from "./user.types";

const find = async (query: Partial<IUser>) => await userModel.find(query).populate("role");

const findOne = async (query: Partial<IUser>) => await userModel.findOne(query).populate("role");

const insertOne = async (data: IUser) => await userModel.create(data);

const insertMany = async (data: IUser[]) => await userModel.insertMany(data);

const findOneAndUpdate = async (findQuery: Partial<IUser>, updateObj: Partial<IUser>) =>
	await userModel.findOneAndUpdate(findQuery, updateObj);

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
};
