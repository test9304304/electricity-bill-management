import { Schema, model } from "mongoose";
import { BaseSchema } from "../utils/base-schema";

import { UserDocument } from "./user.types";

const userSchema = new BaseSchema({
	role: {
		type: Schema.Types.ObjectId,
		ref: "roles",
		required: true,
		default: "user",
	},
	fullName: { type: String, required: true },
	email: {
		type: String,
		required: true,
		trim: true,
		lowercase: true,
		unique: true,
		match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, "Enter a valid email"],
	},
	password: { type: String, required: true },
	location: { type: String, required: true, default: null },
});

export const userModel = model<UserDocument>("users", userSchema);
