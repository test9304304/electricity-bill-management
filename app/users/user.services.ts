import userRepo from "./user.repo";

import { IUser, UserDocument } from "./user.types";
import { userResponses } from "./user.responses";
import { ticketResponses } from "../tickets/ticket.responses";
import ticketServices from "../tickets/ticket.services";

const find = async (query: Partial<IUser>) => await userRepo.find(query);

async function findOne(query: Partial<IUser>, safe?: false): Promise<UserDocument>;
async function findOne(query: Partial<IUser>, safe?: true): Promise<UserDocument | false>;
async function findOne(query: Partial<IUser>, safe: boolean = false) {
	const result = await userRepo.findOne(query);

	if (!result) {
		if (safe) return false;
		throw userResponses.NOT_FOUND;
	}

	return result as UserDocument;
}

const insertOne = async (data: IUser) => {
	try {
		const result = await userRepo.insertOne(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw userResponses.INSERT_FAILED;
	}
};

const insertMany = async (data: IUser[]) => {
	try {
		const result = await userRepo.insertMany(data);
		return result;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw userResponses.INSERT_FAILED;
	}
};

const findOneAndUpdate = async (findQuery: Partial<IUser>, updateObj: Partial<IUser>) => {
	try {
		const result = await userRepo.findOneAndUpdate(findQuery, updateObj);
		return userResponses.UPDATE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw userResponses.UPDATE_FAILED;
	}
};

const deleteOne = async (query: Partial<IUser>) => {
	try {
		const result = await userRepo.findOneAndUpdate(query, { isDeleted: true });
		return userResponses.DELETE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw userResponses.DELETE_SUCCESSFUL;
	}
};

const restoreUser = async (query: Partial<IUser>) => {
	try {
		const result = await userRepo.findOneAndUpdate(query, { isDeleted: false });
		return userResponses.UPDATE_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw userResponses.UPDATE_FAILED;
	}
};

const raiseTicket = async (userId: string, billId: string, issueDescription: string) => {
	try {
		const ticket = { userId, billId, issueDescription };
		const result = await ticketServices.insertOne(ticket);
		return ticketResponses.INSERT_SUCCESSFUL;
	} catch (error: any) {
		if (error.statusCode) throw error;
		throw ticketResponses.INSERT_FAILED;
	}
};

export default {
	find,
	findOne,
	insertOne,
	insertMany,
	findOneAndUpdate,
	deleteOne,
	restoreUser,
	raiseTicket,
};
