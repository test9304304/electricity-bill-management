import z from "zod";
import { Document } from "mongoose";
import { ZBase } from "../utils/base-schema";

export const ZUser = ZBase.extend({
	role: z.string().optional(),
	fullName: z.string(),
	email: z.string().email().trim(),
	password: z.string(),
	location: z.string(),
});

export interface IUser extends z.infer<typeof ZUser> {}
export type UserDocument = Document & IUser;
