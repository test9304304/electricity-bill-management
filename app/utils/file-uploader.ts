import { Request } from "express";
import multer, { FileFilterCallback } from "multer";

const storage = multer.diskStorage({
	destination: (req, file, callback) => callback(null, "../../files/meterImages/"),
	filename: (req, file, callback) => callback(null, `${Date.now()}-${file.originalname}`),
});

const fileFilter = (req: Request, file: Express.Multer.File, callback: FileFilterCallback) => {
	if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
		callback(null, true);
	} else {
		callback(null, false);
	}
};

export const upload = multer({ storage, fileFilter });
