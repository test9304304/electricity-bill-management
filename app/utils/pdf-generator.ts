import PDFDocument from "pdfkit";
import fs from "fs";
import path from "path";

import { IBillData } from "../bills/bill.types";

export const generatePDF = (billData: IBillData) => {
	const { billId, userId, meterId, unitsConsumed, baseRate, discount, totalAmount } = billData;

	const document = new PDFDocument();
	const filePath = path.join(__dirname, "../..", "files/bills", `${billId}.pdf`);

	document.pipe(fs.createWriteStream(filePath));

	document.fontSize(25).text("Electricity Bill", {
		align: "center",
	});

	document
		.fontSize(16)
		.text(`Bill ID: ${billId}`, {
			align: "left",
		})
		.text(`User ID: ${userId}`, {
			align: "left",
		})
		.text(`Meter ID: ${meterId}`, {
			align: "left",
		})
		.text(`Units Consumed: ${unitsConsumed}`, {
			align: "left",
		})
		.text(`Base Rate: Rs.${baseRate}`, {
			align: "left",
		})
		.text(`Discount: ${discount}%`, {
			align: "left",
		})
		.text(`Total Amount: ${totalAmount}`, {
			align: "left",
		});

	document.end();

	return filePath;
};
