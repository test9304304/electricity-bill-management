import nodemailer from "nodemailer";

export const sendBillEmail = async (userEmail: string, billPDFName: string, pdfPath: string) => {
	const { SMTP_PASS } = process.env;
	const transporter = nodemailer.createTransport({
		host: "live.smtp.mailtrap.io",
		port: 587,
		auth: {
			user: "api",
			pass: SMTP_PASS,
		},
	});

	const mailOptions = {
		from: "no-reply-mail@demomailtrap.com",
		to: userEmail,
		subject: "Your Electricity Bill",
		text: "Please find your electricity bill attached.",
		attachments: [
			{
				filename: `${billPDFName}.pdf`,
				path: pdfPath,
			},
		],
	};

	try {
		await transporter.sendMail(mailOptions);
		return true;
	} catch (error) {
		throw `ERROR SENDING MAIL: 
        ${error}`;
	}
};
